#! /bin/bash
# 
# We assume boths a logs and tests directory is available and is run within the context
# of the os161 docker container

set -e

source /helpers.sh

/assignments/$1/run.sh $1

PYTHONDONTWRITEBYTECODE=TRUE /evalaute $LOGS/test_public.log /assignments/$1
