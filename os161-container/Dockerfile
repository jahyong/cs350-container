FROM ubuntu:18.04

# Update gcc - ubuntu is used cause seems alpine gets upset
RUN apt-get update && \
    apt-get install --no-install-recommends -y gcc make wget g++ python3 python3.6-dev libncurses-dev python3-pip git vim bmake build-essential libmpc-dev && \
    rm -rf /var/lib/apt/lists/*

# Setup directories
RUN mkdir -p /os161/tools/bin; \
    mkdir -p /os161/tools/share/man/man1; \
    mkdir -p /os161/tools/share/mk;

# Install a newer version of config.guess with ARM support. Pin it to a specific commit for stability.
ADD https://git.savannah.gnu.org/cgit/config.git/plain/config.guess?id=20403c5701973a4cbd7e0b4bbeb627fcd424a0f1 /tmp/config.guess
RUN chmod +x /tmp/config.guess

COPY dependencies/os161-binutils.tar.gz /binutils.tar.gz
RUN mkdir /binutils-src; \
    tar -xf /binutils.tar.gz -C /binutils-src --strip-components 1
WORKDIR /binutils-src

RUN cp -f /tmp/config.guess .
RUN find . -name '*.info' | xargs touch; \
    ./configure --nfp --disable-werror --target=mips-harvard-os161 --prefix=/os161/tools; \
    make -j 4 && make install; \
    rm -rf /binutils-src; 


# Build GCC
ENV PATH="${PATH}:/os161/tools/bin"

COPY dependencies/os161-gcc.tar.gz /gcc.tar.gz
RUN mkdir /gcc-src; \
    tar -xf /gcc.tar.gz -C /gcc-src --strip-components 1
WORKDIR /gcc-src

RUN cp -f /tmp/config.guess .
RUN CFLAGS="-std=gnu89" ./configure -nfp --disable-shared --disable-threads \
	--disable-libmudflap --disable-libssp --target=mips-harvard-os161 --prefix=/os161/tools; \
    make -j 4 && make install; \
    rm -rf /gcc.tar.gz /gcc-src;

# Make bmake
WORKDIR /
COPY dependencies/os161-mk.tar.gz /mk.tar.gz
COPY dependencies/os161-bmake.tar.gz /bmake.tar.gz
RUN tar -xzvf /bmake.tar.gz

WORKDIR /bmake
RUN tar -xvzf ../mk.tar.gz && chmod u+x mk/install-mk; \
    ./boot-strap --prefix=/os161/tools -m /mk; \
    cp Linux/bmake /os161/tools/bin/ && mkdir -p /os161/tools/share/man/cat1; \
    cp bmake.cat1 /os161/tools/share/man/cat1/bmake.1; \
    sh mk/install-mk /os161/tools/share/mk; \
    rm -rf /bmake.tar.gz /bmake /mk.tar.gz;

# Build sys161
COPY dependencies/sys161.tar.gz /sys161.tar.gz
RUN mkdir /sys161-src
RUN tar -xf /sys161.tar.gz -C /sys161-src --strip-components 1; \
    cd /sys161-src && ./configure --prefix=/os161/tools mipseb && make -j 4 && make install; \
    rm /sys161.tar.gz

# Clean up
WORKDIR /
COPY dependencies/os161.tar.gz /os161.tar.gz
RUN tar -xvzf /os161.tar.gz && rm /os161.tar.gz

COPY dependencies/os161-gdb.tar.gz /os161-gdb.tar.gz
RUN mkdir /os161-gdb/ && tar -xf /os161-gdb.tar.gz -C /os161-gdb --strip-components 1

WORKDIR /os161-gdb
RUN cp -f /tmp/config.guess .
RUN CFLAGS="-std=gnu89" ./configure --target=mips-harvard-os161 --prefix=/os161/tools --disable-werror --with-python=/usr/bin/python3 && \
    make && make install

WORKDIR /
RUN rm -rf /os161-gdb /os161-gdb.tar.gz

RUN rm -f /tmp/config.guess
RUN cd /os161/tools/bin && sh -c 'for i in mips-*; do ln -s /os161/tools/bin/$i /os161/tools/bin/cs350-`echo $i | cut -d- -f4-`; done'

RUN python3 -m pip install -U prettytable
RUN apt update && apt install sudo
COPY evaluate.py /evalaute
RUN useradd -ms /usr/sbin/nologin user
COPY helpers.sh /helpers.sh
RUN echo "source /helpers.sh" >> ~/.bashrc
COPY run_assignment.sh /run_assignment.sh
COPY dependencies/sys161.conf /sys161.conf
COPY assignments /assignments

# In the future I would like to run the build_run_kernel.sh right away - currently we use run.sh to start and exec.
# There is currently an issue where when you build_run_kernel.sh as the entrypoint, when we finally try to run the 
# tests, sys161 will start but almost immediately exit. To hack around this
# we sleep instead and call docker exec to run it.
