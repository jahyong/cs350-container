#!/usr/bin/env bash
export MSYS_NO_PATHCONV=1
realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

rm -rf "logs"

set -e
if [ "$#" -ne 1 ];then
    echo "Usage: run.sh <os161_dir>"
    exit 1
fi


mkdir -p "logs"
chmod a+rwx "logs"
KERNFILE="$1"
docker run -it --rm \
	-v "`realpath "$KERNFILE"`":/kernel \
	-v "`realpath "os161-container/assignments"`":/assignments \
	-v "`realpath "logs"`":/logs \
	--entrypoint bash \
	--name os161 \
	os161-runner

docker kill os161 > /dev/null 2> /dev/null
export MSYS_NO_PATHCONV=0
